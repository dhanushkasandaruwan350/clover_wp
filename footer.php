<?php wp_footer();?>
<footer class="footer-part">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-lg-4">
                <div class="footer-content"><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"></a>
                    <p>For information, here is how you can contact us...</p>
                    <ul class="footer-icon">
                        <li><a class="icon icon-inline" href="#"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a class="icon icon-inline" href="#"><i class="fab fa-twitter"></i></a></li>
                        <li><a class="icon icon-inline" href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a class="icon icon-inline" href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a class="icon icon-inline" href="#"><i class="fab fa-pinterest-p"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-6 col-lg-4">
                <div class="footer-content"><h3 class="title">Useful Links</h3>
                    <div class="footer-widget">
                        <ul>
                            <li><a href="#">My Account</a></li>
                            <li><a href="#">Order History</a></li>
                            <li><a href="#">Order Tracking</a></li>
                            <li><a href="#">Best Seller</a></li>
                            <li><a href="#">New Arrivals</a></li>
                        </ul>
                        <ul>
                            <li><a href="#">Location</a></li>
                            <li><a href="#">Affiliates</a></li>
                            <li><a href="#">Contact</a></li>
                            <li><a href="#">Carrer</a></li>
                            <li><a href="#">Faq</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4">
                <div class="footer-content"><h3 class="title">Download the App</h3>
                     <p>You can now order online from VEGANA using your own smartphone,we'll be delighted to confirm your order in real-time.Simply download the app and start ordering online from VEGANA.<br><br>Depending on your device, you should choose to install the VEGANA Android mobile app or the VEGANA iso mobile app..</p></div>
                <ul class="download-app">
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/google-store.png" alt="google"></a></li>
                    <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/app-store.png" alt="app"></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
<div class="footer-bottom">
    <div class="container"><p>Copyright &copy; 2020 | All rights reserved by - <a target="_blank"
                                                                                  href="https://themeforest.net/user/mironcoder">Mironcoder</a>
        </p>
        <ul class="pay-card">
            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/pay-card/01.jpg" alt="payment-1"></a></li>
            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/pay-card/02.jpg" alt="payment-2"></a></li>
            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/pay-card/03.jpg" alt="payment-3"></a></li>
            <li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/pay-card/04.jpg" alt="payment-4"></a></li>
        </ul>
    </div>
</div>
</body>
</html>
