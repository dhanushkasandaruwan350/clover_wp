<div id="main">
    <?php get_header(); ?>
    <div id="home">
        <?php get_template_part('includes/home', 'content'); ?>
    </div>
    <div id="contact">
        <?php get_template_part('includes/contact', 'content'); ?>
    </div>
    <div id="cart">
        <?php get_template_part('includes/cart', 'content'); ?>
    </div>
    <div id="products">
        <?php get_template_part('includes/products', 'content'); ?>
    </div>
    <div id="about">
        <?php get_template_part('includes/about', 'content'); ?>
    </div>
    <div id="login">
        <?php get_template_part('includes/login', 'content'); ?>
    </div>
    <div id="wish">
        <?php get_template_part('includes/wish', 'content'); ?>
    </div>
    <div id="account">
        <?php get_template_part('includes/account', 'content'); ?>
    </div>
    <div id="checkout">
        <?php get_template_part('includes/checkout', 'content'); ?>
    </div>
    <?php get_footer(); ?>
</div>