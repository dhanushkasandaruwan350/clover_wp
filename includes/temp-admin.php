<!DOCTYPE html>
<html lang="en">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/><!-- /Added by HTTrack -->
<head>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/fonts/flaticon/flaticon.css">
    <link rel="stylesheet" href="../../../../cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/vendor/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/main.css">
    <link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/account.css">
    <script src="<?php echo get_template_directory_uri(); ?>/js/api_access.js"></script>
</head>
<script>
    var products;
    var endpoint = '<?php echo admin_url('admin-ajax.php');?>';
    var db_image;

    function set_all_tab_invisible() {
        document.getElementById('inventory').style.display = 'none';
        document.getElementById('dash').style.display = 'none';
        document.getElementById('pages').style.display = 'none';
        document.getElementById('cates').style.display = 'none';
    }

    function change_tab(name) {
        set_all_tab_invisible();
        if (name === 'inventory') {
            document.getElementById('inventory').style.display = 'block';
            load_products();
        } else if (name === 'dash') {
            document.getElementById('dash').style.display = 'block';
        } else if (name === 'pages') {
            document.getElementById('pages').style.display = 'block';
        } else if (name === 'cates') {
            document.getElementById('cates').style.display = 'block';
        }
        window.scrollTo(0, 0);
    }

    async function load_products() {
        var endpoint = '<?php echo admin_url('admin-ajax.php');?>';
        var key = await get_key_value('KEY', endpoint);
        var token = await get_key_value('TOKEN', endpoint);
        await api_setup_shop_id(key.data);
        await api_setup_shop_token(token.data);

        products = await fetch_items();
        for (let i = 0; i < products.elements.length; i++) {
            if (i == 10) {
                break
            }
            try {
                db_image = await get_product_image(products.elements[i].id, endpoint);
                products.elements[i].image = db_image.data.image
            } catch (e) {
                products.elements[i].image = '<?php echo get_template_directory_uri(); ?>/images/ad/01.jpg';
            }
            $('#admin_products_tbl').append('<tr><td class="table-order"><p>' + products.elements[i].name + '</p></td>' +
                '<td class="table-date"><p>' + products.elements[i].cost + '</p></td><td class="table-status"><p>' + products.elements[i].price + '</p></td>' +
                '<td class="table-product"><p>' + products.elements[i].stockCount + '</p></td><td class="table-total">' +
                '<img src="' + products.elements[i].image + '"alt="product"style="height: 50px"></td>' +
                '<td class="table-action"><div class="order-btn" onclick="selectImage(' + i + ')" style="height: 50px"><a href="#" class="btn btn-inline"><i class="fas fa-eye"></i>Update Image</a></div>');
        }
    }

    function selectImage(i) {
        let image = prompt('Upload Your Image To Media And Past Url Here!' + i);
        var formdata = new FormData;
        formdata.append('action', 'image_entry');
        formdata.append('product', products.elements[i].id);
        formdata.append('image', image);
        $.ajax(endpoint, {
            type: 'POST',
            data: formdata,
            processData: false,
            contentType: false,
            success: function () {
                alert('You have successfully uploaded image for : ' + products.elements[i].name);
            },
            error: function (error) {
                console.log(error)
                alert('we have an error!');
            }
        })
    }
</script>
<body>
<section class="account-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="account-menu">
                    <ul class="nav nav-tabs">
                        <li onclick="change_tab('dash')"><a style="cursor: pointer" class="nav-link active"
                                                            data-toggle="tab">Key Values</a></li>
                        <li onclick="change_tab('inventory')"><a style="cursor: pointer" class="nav-link"
                                                                 data-toggle="tab">Inventory</a></li>
                        <li onclick="change_tab('pages')"><a style="cursor: pointer" class="nav-link" data-toggle="tab">Customize
                                Pages</a></li>
                        <li onclick="change_tab('cates')"><a style="cursor: pointer" class="nav-link" data-toggle="tab">Category
                                And Items</a></li>
                        <li onclick="change_tab('cates')"><a style="cursor: pointer" class="nav-link">Checkout
                                Settings</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="tab-pane active" id="dash">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading"><h2>Primary Settings</h2></div>
                </div>
                <div class="col-lg-6">
                    <div class="account-card">
                        <div class="account-title"><h3>Clover Store Information</h3><a href="#">Edit</a></div>
                        <form class="contact-form" action="#" method="post" id="key_values">
                            <label class="contact-label" style="width: 100%;margin-bottom: 5px">
                                <input type="text" placeholder="STORE ID" style="width: 100%" id="key">
                                <i class="fas fa-user"></i>
                            </label>
                            <label class="contact-label" style="width: 100%;margin-bottom: 5px">
                                <input type="text" placeholder="TOKEN" style="width: 100%" id="token">
                                <i class="fas fa-user"></i>
                            </label>
                            <button class="btn btn-inline" type="submit"><i
                                        class="fas fa-paper-plane"></i><span>SAVE STORE VALUES</span>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="inventory">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading"><h2>Update Products</h2></div>
                </div>
                <div class="col-lg-12">
                    <div class="order-content">
                        <table class="table-list">
                            <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Cost</th>
                                <th scope="col">Price</th>
                                <th scope="col">Stock</th>
                                <th scope="col">Image</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody id="admin_products_tbl" style="height: 30%;overflow: scroll">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="pages">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading"><h2>Order History</h2></div>
                </div>
                <div class="col-lg-12">
                    <div class="order-content">
                        <table class="table-list">
                            <thead>
                            <tr>
                                <th scope="col">Order</th>
                                <th scope="col">Date</th>
                                <th scope="col">Status</th>
                                <th scope="col">Product</th>
                                <th scope="col">Total</th>
                                <th scope="col">Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="table-order"><p>01</p></td>
                                <td class="table-date"><p>May 10, 2018</p></td>
                                <td class="table-status"><p>Completed</p></td>
                                <td class="table-product"><p>4 Item</p></td>
                                <td class="table-total"><p>$32.00</p></td>
                                <td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a href="#"><i
                                                class="fas fa-trash-alt"></i></a></td>
                            </tr>
                            <tr>
                                <td class="table-order"><p>02</p></td>
                                <td class="table-date"><p>May 10, 2018</p></td>
                                <td class="table-status"><p>Completed</p></td>
                                <td class="table-product"><p>4 Item</p></td>
                                <td class="table-total"><p>$32.00</p></td>
                                <td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a href="#"><i
                                                class="fas fa-trash-alt"></i></a></td>
                            </tr>
                            <tr>
                                <td class="table-order"><p>03</p></td>
                                <td class="table-date"><p>May 10, 2018</p></td>
                                <td class="table-status"><p>Completed</p></td>
                                <td class="table-product"><p>4 Item</p></td>
                                <td class="table-total"><p>$32.00</p></td>
                                <td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a href="#"><i
                                                class="fas fa-trash-alt"></i></a></td>
                            </tr>
                            <tr>
                                <td class="table-order"><p>04</p></td>
                                <td class="table-date"><p>May 10, 2018</p></td>
                                <td class="table-status"><p>Processing</p></td>
                                <td class="table-product"><p>4 Item</p></td>
                                <td class="table-total"><p>$32.00</p></td>
                                <td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a href="#"><i
                                                class="fas fa-trash-alt"></i></a></td>
                            </tr>
                            <tr>
                                <td class="table-order"><p>05</p></td>
                                <td class="table-date"><p>May 10, 2018</p></td>
                                <td class="table-status"><p>Processing</p></td>
                                <td class="table-product"><p>4 Item</p></td>
                                <td class="table-total"><p>$32.00</p></td>
                                <td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a href="#"><i
                                                class="fas fa-trash-alt"></i></a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-lg-12">
                    <div class="order-btn"><a href="#" class="btn btn-inline"><i class="fas fa-eye"></i>show more</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="tab-pane" id="cates">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-heading"><h2>User Information</h2></div>
                    <form class="settings-form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group"><label for="fname" class="form-label">First Name:</label><input
                                            type="text" id="fname" class="form-control" placeholder="Mahmudul"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="lname" class="form-label">Last Name:</label><input
                                            type="text" id="lname" class="form-control" placeholder="Hasan"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group"><label for="cname" class="form-label">Company
                                        Name:</label><input type="text" id="cname" class="form-control"
                                                            placeholder="Vegana food world limited"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group"><label for="address" class="form-label">Address:</label><input
                                            type="text" id="address" class="form-control"
                                            placeholder="1420, West Jalkuri, Narayanganj, Bangladesh"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="city" class="form-label">City:</label><input
                                            type="text" id="city" class="form-control" placeholder="Narayanganj"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="state" class="form-label">State:</label><select
                                            id="state" class="form-control">
                                        <option selected>Choose...</option>
                                        <option>...</option>
                                    </select></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="pcode" class="form-label">Post Code:</label><input
                                            type="text" id="pcode" class="form-control" placeholder="1420"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="country" class="form-label">Country:</label><input
                                            type="text" id="country" class="form-control" placeholder="Bangladesh">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="email" class="form-label">Email:</label><input
                                            type="text" id="email" class="form-control"
                                            placeholder="mironcoder@gmail.com">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="pnumber" class="form-label">Phone
                                        Number:</label><input type="text" id="pnumber" class="form-control"
                                                              placeholder="+8801838288389"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="bdate" class="form-label">Birthday:</label><input
                                            type="date" id="bdate" class="form-control" value="1995-02-02"></div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group"><label for="file" class="form-label">Profile
                                        Image:</label><input type="file" id="file" class="form-control"></div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-btn">
                                    <button type="submit" class="btn btn-inline"><i class="fas fa-user-check"></i><span>update info</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/db_access.js"></script>
<script type="application/javascript">
    $('#key_values').on('submit', function (e) {
        e.preventDefault();
        var form = $(this);
        var key = form.find('#key').val();
        var token = form.find('#token').val();
        var endpoint = '<?php echo admin_url('admin-ajax.php');?>';
        save_key_values('KEY', key, endpoint);
        save_key_values('TOKEN', token, endpoint);
    })
</script>
</body>
</html>