<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
body, html {
  height: 100%;
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.hero-image {
  background-image: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("<?php echo get_template_directory_uri(); ?>/images/banner/gro.jpg");
  height: 50%;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  position: relative;
}

.hero-text {
  text-align: center;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
  color: white;

}
div.background {
  background: url(klematis.jpg) repeat;
  border: 0px solid black;
  box-shadow:0 10px 30px rgba(0,0,0,0.5);
  width:50%;
margin:justify;
  height:250px;
  
}

div.transbox {
  margin: 30px;
  background-color: #ffffff;
  border: 0px solid black;
  opacity: 0.6;
}

div.transbox p {
  margin: 5%;
  font-weight: bold;
  color: #000000;
}
.div3 {
  background: green;

}
.right {
  position: absolute;
  right: 50px;
  width: 300px;
  border: 3px solid #73AD21;
  padding: 10px;
  margin: 190px;
  height: 20%;
}
</style>
</head>
<body>

<div class="hero-image">
  <div class="hero-text">
    <h1 style="font-size:50px"> <img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="Flower" width="200" height="55"></h1>
    <p><h1>Online Shopping...</h1></p>
    
  </div>
</div>
<div class="right">
  <img src="<?php echo get_template_directory_uri(); ?>/images/banner/about.jpg" alt="Flower" width="350" height="250">
</div>

       
<div class="background">
  <div class="transbox">
    <p> <br>
    Welcome to Clover Store, your number one source for Food Delivery. We're dedicated to giving you the very best of Vegan Food, with a focus on dependability, customer service and uniqueness.
Founded in 2020 by Mr. Peter Parker, Clover Store has come a long way from its beginning. When Mr. Peter Parker first started out, his passion for Vegan Food drove him to his quit her day job, and gave him the impetus to turn hard work and inspiration into to a booming online store. We now serve customers all over Canada, and are thrilled to be a part of the eco-friendly wing of the food industry.

We hope you enjoy our products as much as we enjoy offering them to you. If you have any questions or comments, please don't hesitate to contact us.

Sincerely,
Name, Peter Parker</p>
  </div>
</div>
<p></p>

</body>
</html>
