<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/checkout.css">
<section class="single-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-content"><h2>Checkout</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item"><a href="product-list-1.html">Shop</a></li>
                        <li class="breadcrumb-item"><a href="product-details-1.html">Shopping Cart</a></li>
                        <li class="breadcrumb-item"><a href="product-details-1.html">Checkout</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="checkout-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="checkout-action"><i class="fas fa-external-link-alt"></i><span>Returning customer? <a
                                href="#">Click here to login</a></span></div>
            </div>
        </div>
        <!--        <form>-->
        <div class="row">
            <div class="col-lg-6">
                <div class="row checkout-form">
                    <div class="col-lg-12">
                        <div class="check-form-title"><h3>Billing Address</h3></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="First Name"
                                                       id="checkout_fname">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Last Name"
                                                       id="checkout_lname">
                        </div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Address"
                                                       id="checkout_address"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="City"
                                                       id="checkout_city"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Post Code"
                                                       id="checkout_postal">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Country"
                                                       id="checkout_country"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="State"
                                                       id="checkout_state"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="email" class="form-control" placeholder="Email"
                                                       id="checkout_email"></div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Phone Number"
                                                       id="checkout_phone">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="row checkout-form">
                    <div class="col-lg-12">
                        <div class="check-form-title"><h3>Payment Method</h3></div>
                    </div>
                    <div class="col-lg-12">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Card Number"
                                                       id="checkout_card_number" value="6011361000006668">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Expire Month"
                                                       id="checkout_exp_month" value="12">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="Expire Year"
                                                       id="checkout_exp_year" value="2021">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <div class="form-group"><input type="text" class="form-control" placeholder="CVV"
                                                       id="checkout_cvv" value="123">
                        </div>
                    </div>
                    <div class="col-lg-12" style="margin-top: 20px">
                        <div class="card text-center">
                            <div class="card-header">Delivery Option</span></div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-lg-6" style="margin-top: 2px">
                                        <button class="btn btn-inline"
                                                onclick="checkout_change_delivery_option('PICKUP')"><i
                                                    class="fas fa-car"></i>
                                            <span>I'll Pickup</span>
                                        </button>
                                    </div>
                                    <div class="col-lg-6" style="margin-top: 2px">
                                        <button class="btn btn-inline"
                                                onclick="checkout_change_delivery_option('HOME DELIVERY')"><i
                                                    class="fas fa-home"></i>
                                            <span>Deliver</span>
                                        </button>
                                    </div>
                                    <div class="col-lg-12" id="checkout_delivery_option_selected"
                                         style="margin-top: 10px;text-align: center;justify-content: center;align-content: center">

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row checkout-table">
            <div class="col-lg-12">
                <div class="check-form-title"><h3>Confirm Order</h3></div>
                <div class="table-scroll">
                    <table class="table-list">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody id="checkout_page_cart_table_body">

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="checkout-charge">
                    <ul id="checkout_page_totals">

                    </ul>
                </div>
                <div class="check-order-btn">
                    <button class="btn btn-inline" onclick="checkout_place_order()"
                    <i class="fas fa-paper-plane"></i>
                    <span>Place order</span>
                    </button>
                </div>
            </div>
        </div>
        <!--        </form>-->
    </div>
</section>
<script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
<div id="scrl"></div>
<div class="row" style="width: 95%;margin-top: 25px">
    <div style="align-content: center;text-align: center;justify-content: center" id="show_confirm" class="col-lg-12">
        <lottie-player src="https://assets3.lottiefiles.com/packages/lf20_thFEdV.json" background="transparent"
                       speed="1"
                       style="width: 500px; height: 450px;margin-left: 35%" loop autoplay></lottie-player>
        <div class="check-order-btn" style="margin-top: 20px;margin-left: 5%">
            <button class="btn btn-inline" onclick="change_page('login')"
            <i class="fas fa-paper-plane"></i>
            <span>View My Orders</span>
            </button>
        </div>
    </div>
    <div style="align-content: center;text-align: center;justify-content: center" id="show_loading" class="col-lg-12">
        <script src="https://unpkg.com/@lottiefiles/lottie-player@latest/dist/lottie-player.js"></script>
        <lottie-player src="https://assets6.lottiefiles.com/datafiles/D7hmWWCXGWSQUPi/data.json"
                       background="transparent"
                       speed="1" style="width: 200px; height: 200px; margin-left: 45%;margin-top: 90px" loop
                       autoplay></lottie-player>
    </div>
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/db_access.js"></script>
<script>
    var delivery_option = 'HOME DELIVERY';
    $('#checkout_delivery_option_selected').append('<span style="font-weight: bold">' + delivery_option + '</span>');
    document.getElementById('show_confirm').style.display = 'none';
    document.getElementById('show_loading').style.display = 'none';

    async function checkout_place_order() {
        var code = document.getElementById("disc").value;

        document.getElementById('show_loading').style.display = 'block';
        document.getElementById("scrl").scrollIntoView();
        //card
        var number = document.getElementById("checkout_card_number").value;
        var exp_month = document.getElementById("checkout_exp_month").value;
        var exp_year = document.getElementById("checkout_exp_year").value;
        var cvv = document.getElementById("checkout_cvv").value;

        //customer
        var fname = document.getElementById("checkout_fname").value;
        var lname = document.getElementById("checkout_lname").value;
        var address = document.getElementById("checkout_address").value;
        var city = document.getElementById("checkout_city").value;
        var postal = document.getElementById("checkout_postal").value;
        var country = document.getElementById("checkout_country").value;
        var state = document.getElementById("checkout_state").value;
        var email = document.getElementById("checkout_email").value;
        var phone = document.getElementById("checkout_phone").value;

        var items = [];
        for (let i = 0; i < cart.length; i++) {
            items.push({
                "tax_rates": [],
                "amount": cart[i].price,
                "currency": "usd",
                "description": cart[i].name,
                "parent": cart[i].id,
                "quantity": cart[i].cart_qty,
                "type": "sku"
            })
        }

        var token = await create_card_token(number, exp_month, exp_year, cvv);
        console.log(token);
        var cust = await create_customer(token.id, city, country, address, postal, state, email, fname, lname, phone);
        console.log(cust);
        var ecom_order = await create_order_from_ecom(items, cust.id, email);
        console.log(ecom_order)
        if (code.length > 1) {
            var discount = await add_discount(code, ecom_order.id);
            console.log(discount);
            var updated_order = await update_order_with_disc(ecom_order.id, delivery_option, code, discount.id, (ecom_order.amount - 50));
            console.log(updated_order);
        } else {
            var updated_order = await update_order(ecom_order.id, delivery_option);
            console.log(updated_order);
        }
        try {
            var payment = await pay_for_order(cust.id, email, updated_order.id);
            console.log(payment);
            var print = await print_order(updated_order.id);
            console.log(print);
        } catch (e) {
            console.log(e);
        }
        document.getElementById("checkout_card_number").value = '';
        document.getElementById("checkout_exp_month").value = '';
        document.getElementById("checkout_exp_year").value = '';
        document.getElementById("checkout_cvv").value = '';


        document.getElementById("checkout_fname").value = '';
        document.getElementById("checkout_lname").value = '';
        document.getElementById("checkout_address").value = '';
        document.getElementById("checkout_city").value = '';
        document.getElementById("checkout_postal").value = '';
        document.getElementById("checkout_country").value = '';
        document.getElementById("checkout_state").value = '';
        document.getElementById("checkout_email").value = '';
        document.getElementById("checkout_phone").value = '';

        cart = [];
        cart_page_load_cart_table();
        document.getElementById('show_loading').style.display = 'none';
        document.getElementById('show_confirm').style.display = 'block';
        document.getElementById("scrl").scrollIntoView();
    }

    function checkout_change_delivery_option(option) {
        delivery_option = option;
        document.getElementById("checkout_delivery_option_selected").innerHTML = "";
        $('#checkout_delivery_option_selected').append('<span style="font-weight: bold">' + delivery_option + '</span>');
    }
</script>