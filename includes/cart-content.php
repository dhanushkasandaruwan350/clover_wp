<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/cartlist.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/api_access.js"></script>
<script>
    var cart_page_sub_total = 0;
    var db_image;

    function cart_page_load_cart_table() {
        var endpoint = '<?php echo admin_url('admin-ajax.php');?>';
        document.getElementById("cart_page_cart_table_body").innerHTML = "";
        document.getElementById("checkout_page_cart_table_body").innerHTML = "";
        document.getElementById("cart_page_totals").innerHTML = "";
        document.getElementById("checkout_page_totals").innerHTML = "";
        cart_page_sub_total = 0;
        qty_on_cart = 0;
        for (let i = 0; i < cart.length; i++) {
            cart_page_sub_total = cart_page_sub_total + (cart[i].price * cart[i].cart_qty);
            qty_on_cart = (parseInt(qty_on_cart) + parseInt(cart[i].cart_qty));

            $('#cart_page_cart_table_body').append('<tr><td class="table-number"><h5>' + (i + 1) + '</h5></td>' +
                '<td class="table-product"><img src="' + cart[i].image + '" alt="product"></td>' +
                '<td class="table-name"><h5>' + cart[i].name + '</h5></td>' +
                '<td class="table-price"><h5>' + cart[i].price + '</h5></td>' +
                '<td class="table-quantity"><input type="number" value="' + cart[i].cart_qty + '" onchange="cart_page_change_qty(' + i + ',this.value)"></td>' +
                '<td class="table-total"><h5>' + (cart[i].price * cart[i].cart_qty) + '</h5></td>' +
                '<td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a style="cursor: pointer" onclick="cart_page_remove_item(' + i + ')">' +
                '<i class="fas fa-trash-alt"></i></a></td></tr>');

            $('#checkout_page_cart_table_body').append('<tr><td class="table-number"><h5>' + (i + 1) + '</h5></td>' +
                '<td class="table-product"><img src="' + cart[i].image + '" alt="product"></td>' +
                '<td class="table-name"><h5>' + cart[i].name + '</h5></td>' +
                '<td class="table-price"><h5>' + cart[i].price + '</h5></td>' +
                '<td class="table-quantity"><input type="number" value="' + cart[i].cart_qty + '" onchange="cart_page_change_qty(' + i + ',this.value)"></td>' +
                '<td class="table-total"><h5>' + (cart[i].price * cart[i].cart_qty) + '</h5></td>' +
                '<td class="table-action"><a href="#"><i class="fas fa-eye"></i></a><a style="cursor: pointer" onclick="cart_page_remove_item(' + i + ')">' +
                '<i class="fas fa-trash-alt"></i></a></td></tr>');
        }
        $('#cart_page_totals').append('<li><span>Shipping Charge</span><span>Free</span></li>' +
            '<li><span>Subtotal</span><span>$' + parseFloat(cart_page_sub_total).toFixed(2) + '</span></li><li><span>Total</span><span>$' + parseFloat(cart_page_sub_total).toFixed(2) + '</span></li>')

        $('#checkout_page_totals').append('<li><span>Shipping Charge</span><span>Free</span></li>' +
            '<li><span>Subtotal</span><span>$' + parseFloat(cart_page_sub_total).toFixed(2) + '</span></li><li><span>Total</span><span>$' + parseFloat(cart_page_sub_total).toFixed(2) + '</span></li>')
        check_code();
        document.getElementById("header_cart_item_count").innerHTML = qty_on_cart;
    }

    function cart_page_remove_item(item) {
        if (item > -1) {
            cart.splice(item, 1);
            cart_page_load_cart_table();
        }
    }

    function cart_page_change_qty(item, new_qty) {
        if (new_qty < 1) {
            cart.splice(item, 1);
        } else {
            cart[item].cart_qty = new_qty;
        }
        cart_page_load_cart_table();
    }

    function check_code() {
        var code = document.getElementById("disc").value
        if (code === 'technography') {
            document.getElementById("cart_page_totals").innerHTML = "";
            document.getElementById("checkout_page_totals").innerHTML = "";
            $('#cart_page_totals').append('<li><span>Delivery Charge</span><span>Free</span></li><li><span>Coupon Discount</span><span>-$50</span></li>' +
                '<li><span>Subtotal</span><span>$' + parseFloat((cart_page_sub_total - 50) + '').toFixed(2) + '</span></li><li><span>Total</span><span>$' + parseFloat((cart_page_sub_total - 50) + '').toFixed(2) + '</span></li>')

            $('#checkout_page_totals').append('<li><span>Delivery Charge</span><span>Free</span></li><li><span>Coupon Discount</span><span>-$50</span></li>' +
                '<li><span>Subtotal</span><span>$' + parseFloat((cart_page_sub_total - 50) + '').toFixed(2) + '</span></li><li><span>Total</span><span>$' + parseFloat((cart_page_sub_total - 50) + '').toFixed(2) + '</span></li>')
            discount = code;

        }
    }

    var form = document.getElementById("disc_form");

    function handleForm(event) {
        event.preventDefault();
    }

    form.addEventListener('submit', check_code());
</script>
<section class="single-banner">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="single-content"><h2>Shopping Cart</h2>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item"><a href="product-list-1.html">Shop</a></li>
                        <li class="breadcrumb-item"><a href="product-details-1.html">Shopping Cart</a></li>
                    </ol>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="cart-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-list">
                    <table class="table-list">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Product</th>
                            <th scope="col">Name</th>
                            <th scope="col">Price</th>
                            <th scope="col">Quantity</th>
                            <th scope="col">Total</th>
                            <th scope="col">Action</th>
                        </tr>
                        </thead>
                        <tbody id="cart_page_cart_table_body">
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-lg-6">
                <div class="cart-back"><a href="https://mironmahmud.com/vegana/assets/product-list-1.html"
                                          class="btn btn-inline"><i
                                class="fas fa-undo-alt"></i><span>Back to Shop</span></a></div>
            </div>
            <div class="col-md-6 col-lg-6">
                <div class="cart-cupon">
                    <form id="disc_form" onsubmit="return false"><input type="text" placeholder="Coupon Code" id="disc">
                        <button class="btn btn-inline" onclick="check_code()"><i
                                    class="fas fa-cut"></i><span>Apply Now</span></button>
                    </form>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="cart-totals"><h2 class="title">Cart Totals</h2>
                    <ul id="cart_page_totals">

                    </ul>
                </div>
                <div class="cart-proceed">
                    <a onclick="change_page('checkout')" class="btn btn-inline">
                        <i class="fas fa-check"></i><span>Proceed to Checkout</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>