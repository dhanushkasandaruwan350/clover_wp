<!-- Slideshow container -->
<div class="slideshow-container">

    <!-- Full-width images with number and caption text -->
    <div class="mySlides fade">
        <div class="numbertext">1 / 3</div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/banner/01.jpg" style="width:100%">
        <div class="text">Caption Text</div>
    </div>

    <div class="mySlides fade">
        <div class="numbertext">2 / 3</div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/banner/02.jpg" style="width:100%">
        <div class="text">Caption Two</div>
    </div>

    <div class="mySlides fade">
        <div class="numbertext">3 / 3</div>
        <img src="<?php echo get_template_directory_uri(); ?>/images/banner/03.jpg" style="width:100%">
        <div class="text">Caption Three</div>
    </div>

    <!-- Next and previous buttons -->
    <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
    <a class="next" onclick="plusSlides(1)">&#10095;</a>
</div>
<br>

<!-- The dots/circles -->
<div style="text-align:center">
    <span class="dot" onclick="currentSlide(1)"></span>
    <span class="dot" onclick="currentSlide(2)"></span>
    <span class="dot" onclick="currentSlide(3)"></span>
</div>
<style type="text/css">
    * {
        box-sizing: border-box
    }

    /* Slideshow container */
    .slideshow-container {
        max-width: 1920px;
        position: relative;
        margin: auto;
    }


    /* Next & previous buttons */
    .prev, .next {
        cursor: pointer;
        position: absolute;
        top: 50%;
        width: auto;
        margin-top: -22px;
        padding: 16px;
        color: white;
        font-weight: bold;
        font-size: 18px;
        transition: 0.6s ease;
        border-radius: 0 3px 3px 0;
        user-select: none;
    }

    /* Position the "next button" to the right */
    .next {
        right: 0;
        border-radius: 3px 0 0 3px;
    }

    /* On hover, add a black background color with a little bit see-through */
    .prev:hover, .next:hover {
        background-color: rgba(0, 0, 0, 0.8);
    }

    /* Caption text */
    .text {
        color: #f2f2f2;
        font-size: 15px;
        padding: 8px 12px;
        position: absolute;
        bottom: 8px;
        width: 100%;
        text-align: center;
    }

    /* Number text (1/3 etc) */
    .numbertext {
        color: #f2f2f2;
        font-size: 12px;
        padding: 8px 12px;
        position: absolute;
        top: 0;
    }

    /* The dots/bullets/indicators */
    .dot {
        cursor: pointer;
        height: 15px;
        width: 15px;
        margin: 0 2px;
        background-color: #bbb;
        border-radius: 50%;
        display: inline-block;
        transition: background-color 0.6s ease;
    }

    .active, .dot:hover {
        background-color: #717171;
    }

    /* Fading animation */
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 5.0s;
        animation-name: fade;
        animation-duration: 5.0s;
    }

    @-webkit-keyframes fade {
        from {
            opacity: .4
        }
        to {
            opacity: 1
        }
    }

    @keyframes fade {
        from {
            opacity: .4
        }
        to {
            opacity: 1
        }
    }

</style>

<script type="text/javascript">
    var slideIndex = 0;
    showSlides();

    function showSlides() {
        var i;
        var slides = document.getElementsByClassName("mySlides");
        for (i = 0; i < slides.length; i++) {
            slides[i].style.display = "none";
        }
        slideIndex++;
        if (slideIndex > slides.length) {
            slideIndex = 1
        }
        slides[slideIndex - 1].style.display = "block";
        setTimeout(showSlides, 5000); // Change image every 2 seconds
    }
</script>

<!-- End of Slideshow container -->

<script src="<?php echo get_template_directory_uri(); ?>/js/api_access.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/custom/product-list-1.css">
<script src="<?php echo get_template_directory_uri(); ?>/js/custom/price-range.js"></script>
<script>
    var db_image;
    var home_res;

    function execute_home_page_scripts() {
        set_homepage_items();
    }

    async function set_homepage_items() {
        home_res = await fetch_items();
        for (let i = 0; i < home_res.elements.length; i++) {
            if (i == 24) {
                $('#home_page_product_list').append('<div class="col-md-12 col-lg-12" style="text-align: center;align-items: center;justify-content: center">' +
                    '<button class="btn btn-inline" onclick="change_page(\'products\');execute_sale_page_scripts();"><i class="fas fa-arrow-circle-right"></i><span>See More</span></button></div>');
                break
            }
            try {
                db_image = await get_product_image(home_res.elements[i].id, endpoint);
                home_res.elements[i].image = db_image.data.image
            } catch (e) {
                home_res.elements[i].image = '<?php echo get_template_directory_uri(); ?>/images/ad/01.jpg';
            }
            $('#home_page_product_list').append('<div class="col-12 col-sm-6 col-md-2 col-lg-2">' +
                '<div class="product-card card-gape">' +
                '<div class="product-img">' +
                '<img src="' + home_res.elements[i].image + '" alt="product">' +
                '<ul class="product-widget">' +
                '<ul class="product-widget">' +
                '<li><button><i class="fas fa-eye"></i></button></li>' +
                '<li><button><i class="fas fa-heart"></i></button></li>' +
                '<li><button><i class="fas fa-exchange-alt"></i></button></li>' +
                '</ul>' +
                '</div>' +
                '<div class="product-content">' +
                '<div class="product-name"><h6><a href="#">' + home_res.elements[i].name + '</a></h6></div>' +
                '<div class="product-price">' +
                '<h6><del>$80</del>$' + home_res.elements[i].price + '</h6>' +
                '<div class="product-rating"><i class="fas fa-star"></i><span>4.5/2</span></div>' +
                '</div>' +
                '<div class="product-btn" onclick="shop_page_add_to_cart(' + i + ')"><a style="cursor: pointer"><i class="fas fa-shopping-basket"></i><span>Add to Cart</span></a>' +
                '</div></div></div></div>'
            )
        }
    }
</script>

<style>
    #home_page_product_list::-webkit-scrollbar {
        width: 0 !important
    }

    #home_page_product_list.element {
        overflow: -moz-scrollbars-none;
    }

    #home_page_product_list.element {
        -ms-overflow-style: none;
    }
</style>

<section>
    <div class="row product-card-parent" id="home_page_product_list"
         style="margin-top: 50px;margin-left: 5px;margin-right: 5px">
    </div>
</section>
<section class="feature-part" style="margin-top: 15px">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-delivery-truck"></i>
                    <h3>Free Delivery</h3>
                    <p>Get things delivered to your Home ...</p></div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-save-money"></i>
                    <h3>Instant Return</h3>
                    <p>once verified, you will get your money back instantly. you can also exchange the item for
                        something else.</p></div>
            </div>
            <div class="col-md-4 col-lg-4">
                <div class="feature-card"><i class="flaticon-customer-service"></i>
                    <h3>Quick Support</h3>
                    <p>24/7 Hours Service...</p></div>
            </div>
        </div>
    </div>
</section>
<section class="news-part">
    <div class="container">
        <div class="row">
            <div class="col-md-5 col-lg-6">
                <div class="news-content"><h2>Subscribe for Latest Offers</h2></div>
            </div>
            <div class="col-md-7 col-lg-6">
                <form class="search-form news-form"><input type="text" placeholder="Enter Email Address">
                    <button class="btn btn-inline"><i class="fas fa-envelope"></i><span>Subscribe</span></button>
                </form>
            </div>
        </div>
    </div>
</section>
