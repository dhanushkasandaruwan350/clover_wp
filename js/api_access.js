var SHOP_ID = '';
var API_KEY = '';
var Authorization_Headers = {
    Accept: 'application/json',
    Authorization: ''
}

function api_setup_shop_id(data) {
    SHOP_ID = data.value
    console.log('SHOP ID SETUP : ' + SHOP_ID)
}

function api_setup_shop_token(data) {
    Authorization_Headers = {
        Accept: 'application/json',
        Authorization: 'Bearer ' + data.value
    }
    API_KEY = 'f46950c471ef47b89339e6aad9894358';
    console.log('SHOP TOKEN SETUP : ' + Authorization_Headers.Authorization)
}

function fetch_categories() {
    const options = {method: 'GET', headers: Authorization_Headers};
    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/categories', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function fetch_items() {
    const options = {method: 'GET', headers: Authorization_Headers};
    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/items', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function fetch_tags() {
    const options = {method: 'GET', headers: Authorization_Headers};
    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/tags', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}


function create_card_token(number, month, year, cvv) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            apikey: API_KEY,
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            card: {number: number, exp_month: month, exp_year: year, cvv: cvv}
        })
    };

    return fetch('https://try.readme.io/https://token-sandbox.dev.clover.com/v1/tokens', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function create_customer(source, city, country, adl1, postal, state, email, fname, lname, phone) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({
            ecomind: 'ecom',
            shipping: {
                address: {city: city, country: country, line1: adl1, postal_code: postal, state: state}
            },
            email: email,
            firstName: fname,
            lastName: lname,
            name: fname + ' ' + lname,
            source: source,
            phone: phone
        })
    };

    return fetch('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/customers', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function create_order_from_ecom(items, customer, email) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: 'Bearer aefc3ccc-34fa-cb34-5a2c-63d0698082d8'
        },
        body: JSON.stringify({
            items,
            currency: 'usd',
            customer: customer,
            email: email
        })
    };

    return fetch('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/orders', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function update_order(order, delivery) {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({
            taxRemoved: 'false',
            id: order,
            note: delivery
        })
    };

    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/88M9MWYTSCH51/orders/' + order, options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function update_order_with_disc(order, delivery, code, disc,tot) {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({
            taxRemoved: 'false',
            id: order,
            discounts: [{discount: {}, approver: {}, id: disc, name: code, amount: 100}],
            note: delivery,
            total: tot
        })
    };

    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/88M9MWYTSCH51/orders/' + order, options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function pay_for_order(customer, email, order) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({
            ecomind: 'ecom',
            customer: customer,
            email: email
        })
    };
    return fetch('https://try.readme.io/https://scl-sandbox.dev.clover.com/v1/orders/' + order + '/pay', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function add_discount(name, order) {
    const options = {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({name: name, amount: -50})
    };

    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/orders/' + order + '/discounts', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}

function print_order(order) {
    const options = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: Authorization_Headers.Authorization
        },
        body: JSON.stringify({orderRef: {id: order}, category: 'ORDER', state: 'PRINTING'})
    };

    return fetch('https://try.readme.io/https://sandbox.dev.clover.com/v3/merchants/' + SHOP_ID + '/print_event', options)
        .then(response => response.json())
        .catch(err => console.error(err));
}